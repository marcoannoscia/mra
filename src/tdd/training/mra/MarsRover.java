package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private List<String> planetObstacles ;
	private int maxCoordinateX ;
	private int maxCoordinateY ;
	private String dir ;
	private int actualX ;
	private int actualY ;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(!checkCoordinatesArePositive(planetX, planetY)) {
			throw new MarsRoverException("Planet coordinates not valid") ;
		}else {
			
			maxCoordinateX = planetX ;
			maxCoordinateY = planetY ;
			this.planetObstacles = planetObstacles ;
			actualX = 0 ;
			actualY = 0 ; 
			dir = "N" ;
			}
	}

	private boolean checkCoordinatesArePositive(int planetX, int planetY) {
		return planetX > 0 && planetY > 0;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		String toCheck = "("+ x +"," + y +")" ;
		
		if(checkValidCoordinates(x, y)) {
			throw new MarsRoverException("Invalid coordinates") ;
		}
		
		if(planetObstacles == null) {
			return false ;
		}else {
			for(String obstacleCoordinate : planetObstacles) {
				if(toCheck.equals(obstacleCoordinate)) {
					return true ;
				}
			}
		}
		
		return false ;
	}

	private boolean checkValidCoordinates(int x, int y) {
		return !checkCoordinatesArePositive(x, y) || x > maxCoordinateX || y > maxCoordinateY;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		if (!checkValidCommand(commandString)) {
			throw new MarsRoverException("Invalid command") ;
		}
		
		if(commandString.length() > 1) {
			String singleCommand = "" ;
			
			for(int i = 0 ; i < commandString.length() ; i++) {
				singleCommand = "" ;
				singleCommand += commandString.charAt(i) ;
				this.executeCommand(singleCommand) ;
			}
		}
		
		if (commandString == "r") {
			executeCommandR();
		}else if(commandString == "l"){
			executeCommandL();
		}else if(commandString == "f") {
			executeCommandF();
		}else if(commandString == "b") {
			executeCommandB();
		}
		
		return "("+ actualX + "," + actualY + "," + dir + ")" ;
	}

	private void executeCommandB() throws MarsRoverException {
		if(dir == "N") {
			if(actualY == 0) {
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualY-- ;
			}
		}else if(dir == "E") {
			if(actualX == 0){
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualX-- ;
			}
		}else if(dir == "S") {
			if(actualY == maxCoordinateY){
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualY++ ;
			}
		}else {
			if(actualX == maxCoordinateX){
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualX++ ;
			}
		}
	}

	private void executeCommandF() throws MarsRoverException {
		if(dir == "N") {
			if(actualY == maxCoordinateY) {
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualY++ ;
			}
		}else if(dir == "E") {
			if(actualX == maxCoordinateX){
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualX++ ;
			}
		}else if(dir == "S") {
			if(actualY == 0){
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualY-- ;
			}
		}else {
			if(actualX == 0){
				throw new MarsRoverException("It's not over possible to move forward") ;
			}else{
				actualX-- ;
			}
		}
	}

	private void executeCommandL() {
		if(dir == "N") {
			dir = "W" ;
		}else if(dir == "W") {
			dir = "S" ;
		}else if(dir == "S") {
			dir = "E" ;
		}else {
			dir = "N" ;
		}
	}

	private void executeCommandR() {
		if(dir == "N") {
			dir = "E" ;
		}else if(dir == "E") {
			dir = "S" ;
		}else if(dir == "S") {
			dir = "W" ;
		}else {
			dir = "N" ;
		}
	}

	private boolean checkValidCommand(String commandString) {
		for(int i = 0 ; i < commandString.length() ; i++) {
			if(commandString.charAt(i) != 'f' && commandString.charAt(i) != 'b' && commandString.charAt(i) != 'l' && commandString.charAt(i) != 'r') {
				return false ;
			}
		}
		
		return true ;
	}

	public void setActualX(int i) {
		this.actualX = i ;
	}

	public void setActualY(int i) {
	 this.actualY = i ;
	}
	
	public void setDir(String direction) throws MarsRoverException {
		 if(direction == "N" || direction == "S" || direction == "E" || direction == "W") {
			this.dir = direction ;
		}else{
			throw new MarsRoverException("Direcction not valid") ;
		}
	}
}
