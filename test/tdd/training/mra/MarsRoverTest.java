package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstaclesAt() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		planetObstacles.add("(4,7)") ;
		planetObstacles.add("(2,3)") ;
		
		assertTrue(rover.planetContainsObstacleAt(4, 7)) ;
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void planetCoordinateXUnder0ShouldThrowMarsRoverException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(-1, 10, planetObstacles) ;
		
	}
	
	@Test(expected = MarsRoverException.class)
	public void planetCoordinateYUnder0ShouldThrowMarsRoverException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, -1, planetObstacles) ;
		
	}
	
	@Test
	public void testPlanetContainsObstaclesAtWithNullPlanetObstacles() throws Exception {
	
		MarsRover rover = new MarsRover(10, 10, null) ;
		assertFalse(rover.planetContainsObstacleAt(4, 7)) ;
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void testContainsObstaclesWithInvalidCoordinatesShouldThrowMarsRoverException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		planetObstacles.add("(4,7)") ;
		planetObstacles.add("(2,3)") ;
		
		boolean founded = rover.planetContainsObstacleAt(11, 7) ;
		
	}
	
	@Test
	public void testExecuteCommand() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		planetObstacles.add("(4,7)") ;
		planetObstacles.add("(2,3)") ;
		
		assertEquals("(0,0,N)", rover.executeCommand("")) ;
		
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void testExecuteCommandWithInvalidCommandShouldThrowMArsRoverException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		planetObstacles.add("(4,7)") ;
		planetObstacles.add("(2,3)") ;
		
		String result = rover.executeCommand("o") ;
		
	}
	
	
	@Test
	public void executeCommandRShouldTurnRoverRight() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
	
		assertEquals("(0,0,E)", rover.executeCommand("r")) ;
	}
	
	
	@Test
	public void executeCommandLShouldTurnRoverLeft() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
	
		assertEquals("(0,0,W)", rover.executeCommand("l")) ;
	}
	
	
	@Test
	public void executeCommandFShouldMovingRoverForwardDirN() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		rover.setActualX(7) ;
		rover.setActualY(6) ;
		
	
		assertEquals("(7,7,N)", rover.executeCommand("f")) ;
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void movingRoverForwardOverPlanetGridBoundsShouldThrowMarsRoverException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 6, planetObstacles) ;
		rover.setActualX(7) ;
		rover.setActualY(6) ;
		
	
		String result = rover.executeCommand("f") ;
	}

	
	@Test
	public void executeCommandFShouldMovingRoverForwardDirE() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		rover.setDir("E");
		rover.setActualX(1);
		rover.setActualY(1);
		
	
		assertEquals("(2,1,E)", rover.executeCommand("f")) ;
	}
	
	@Test
	public void executeCommandFShouldMovingRoverForwardDirS() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		rover.setDir("S");
		rover.setActualX(1);
		rover.setActualY(1);
		
	
		assertEquals("(1,0,S)", rover.executeCommand("f")) ;
	}
	
	@Test(expected = MarsRoverException.class)
	public void executeCommandFWihtDirSAndCoordinates20ShouldThrowException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		rover.setDir("S");
		rover.setActualX(2);
		
	
		String result = rover.executeCommand("f") ;
	}
	
	
	@Test
	public void executeCommandFShouldMovingRoverForwardDirW() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		rover.setDir("W");
		rover.setActualX(1);
		rover.setActualY(1);
		
	
		assertEquals("(0,1,W)", rover.executeCommand("f")) ;
	}
	
	
	
	@Test(expected = MarsRoverException.class)
	public void executeCommandFWihtDirWAndCoordinates02ShouldThrowException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
		rover.setDir("W");
		rover.setActualY(2);
		
	
		String result = rover.executeCommand("f") ;
	}
	
	
	@Test
	public void executeCommandBShouldMovingRoverBackwardDirN() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		rover.setActualX(5) ;
		rover.setActualY(8) ;
		rover.setDir("E");
		
	
		assertEquals("(4,8,E)", rover.executeCommand("b")) ;
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void executeCommandBWihtDirNAndCoordinates00ShouldThrowException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		
	
		String result = rover.executeCommand("b") ;
	}
	
	@Test(expected = MarsRoverException.class)
	public void executeCommandBWihtDirEAndCoordinates00ShouldThrowException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		rover.setDir("E");
	
		String result = rover.executeCommand("b") ;
	}
	
	
	@Test(expected = MarsRoverException.class)
	public void executeCommandBWihtDirSAndCoordinates010ShouldThrowException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		rover.setDir("S");
		rover.setActualY(10);
	
		String result = rover.executeCommand("b") ;
	}
	
	@Test(expected = MarsRoverException.class)
	public void executeCommandBWihtDirWAndCoordinates100ShouldThrowException() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;
		rover.setDir("W");
		rover.setActualX(10);
	
		String result = rover.executeCommand("b") ;
	}
	
	
	@Test
	public void testExecuteMultipleCommands() throws Exception {
		List<String>planetObstacles = new ArrayList<>() ;
		MarsRover rover = new MarsRover(10, 10, planetObstacles) ;

	
		assertEquals("(2,2,E)", rover.executeCommand("ffrff")) ;
	}
}
